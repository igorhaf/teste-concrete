"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const bcrypt = require("bcrypt");
const PhoneSchema = new mongoose_1.Schema({
    numero: String,
    ddd: String,
});
exports.userSchema = new mongoose_1.Schema({
    nome: String,
    email: String,
    senha: String,
    createdAt: Date,
    telefones: [PhoneSchema],
});
exports.userSchema.pre("save", function (next) {
    if (!this.createdAt) {
        this.createdAt = new Date();
    }
    this.senha = bcrypt.hashSync(this.senha, 10);
    next();
});
