"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
class IndexRoute {
    static create(router) {
        router.post("/signup", (req, res, next) => {
            new IndexRoute().signup(req, res, next);
        });
        router.post("/authenticate", (req, res, next) => {
            new IndexRoute().authenticate(req, res, next);
        });
        router.get("/search", (req, res, next) => {
            new IndexRoute().search(req, res, next);
        });
    }
    signup(req, res, next) {
        const User = req.app.get("UserModel");
        User.find({ email: req.body.email }, function (err, docs) {
            if (docs.length) {
                res.json({ status: 200, message: "Email ja cadastrado" });
            }
            else {
                User.create(req.body, function (err, doc) {
                    if (err) {
                        throw err;
                    }
                    else {
                        const payload = {
                            email: doc.email,
                            nome: doc.nome,
                            _id: doc._id,
                        };
                        const jwttoken = jwt.sign({ email: doc.email, nome: doc.nome, _id: doc._id }, req.app.get("jwt-secret"), { expiresIn: 1800 });
                        res.json({
                            status: 200,
                            message: {
                                id: doc._id,
                                nome: doc.nome,
                                email: doc.email,
                                telefones: doc.telefones,
                                token: jwttoken,
                            },
                        });
                    }
                });
            }
        });
    }
    search(req, res, next) {
        const authorization = req.headers.authorization;
        if (req.headers && authorization && authorization.split(" ")[0] === "Bearer") {
            jwt.verify(authorization.split(" ")[1], req.app.get("jwt-secret"), function (err, decode) {
                if (err) {
                    err = {
                        name: "TokenExpiredError",
                        message: "Sessão inválida",
                    };
                    res.json({ success: false, message: err });
                }
                if (decode) {
                    const User = req.app.get("UserModel");
                    User.findOne({
                        email: decode.email,
                    }, function (err, doc) {
                        if (err) {
                            throw err;
                        }
                        else {
                            res.json({
                                status: 200,
                                message: {
                                    id: doc._id,
                                    nome: doc.nome,
                                    email: doc.email,
                                    telefones: doc.telefones,
                                },
                            });
                        }
                    });
                }
            });
        }
        else {
            res.json({ status: 401, message: "Não Autorizado" });
        }
    }
    authenticate(req, res, next) {
        const User = req.app.get("UserModel");
        User.findOne({
            email: req.body.email,
        }, function (err, user) {
            if (err) {
                throw err;
            }
            if (!user) {
                res.json({ status: 401, message: "Usuário não encontrado" });
            }
            else if (user) {
                if (!bcrypt.compareSync(req.body.senha, user.senha)) {
                    res.json({ status: 401, message: "Usuário e/ou senha inválidos" });
                }
                else {
                    const payload = {
                        email: user.email,
                        nome: user.nome,
                        _id: user._id,
                    };
                    const jwttoken = jwt.sign({ email: user.email, nome: user.nome, _id: user._id }, req.app.get("jwt-secret"), { expiresIn: 1800 });
                    res.json({
                        status: 200,
                        message: { token: jwttoken },
                    });
                }
            }
        });
    }
}
exports.IndexRoute = IndexRoute;
