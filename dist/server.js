"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = require("body-parser");
const errorHandler = require("errorhandler");
const express = require("express");
const methodOverride = require("method-override");
const mongoose = require("mongoose");
const logger = require("morgan");
const path = require("path");
const index_1 = require("./routes/index");
const user_1 = require("./schemas/user");
class Server {
    static bootstrap() {
        return new Server();
    }
    constructor() {
        this.model = Object();
        this.app = express();
        this.config();
        this.routes();
        this.api();
    }
    api() {
    }
    config() {
        const MONGODB_CONNECTION = "mongodb://teste:concrete@ds133557.mlab.com:33557/teste-concrete";
        this.app.use(express.static(path.join(__dirname, "public")));
        this.app.set("views", path.join(__dirname, "views"));
        this.app.set("view engine", "pug");
        this.app.set("jwt-secret", "concret");
        this.app.use(logger("dev"));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({
            extended: true,
        }));
        this.app.use(methodOverride());
        global.Promise = require("q").Promise;
        mongoose.Promise = global.Promise;
        const connection = mongoose.createConnection(MONGODB_CONNECTION, { useMongoClient: true });
        this.app.set("UserModel", connection.model("User", user_1.userSchema));
        this.app.use(function (err, req, res, next) {
            err.status = 404;
            next(err);
        });
        this.app.use(errorHandler());
    }
    routes() {
        let router;
        router = express.Router();
        index_1.IndexRoute.create(router);
        this.app.use(router);
    }
}
exports.Server = Server;
