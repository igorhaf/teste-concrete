# Teste Concrete Solutions

## Link para o projeto no Heroku

https://murmuring-gorge-30759.herokuapp.com

## Instalação

Instalação dos pacotes NPM

`$ npm install`

Build via grunt para geração de dist a partir do TypeScript e verificação pelo TSLint:

`$ npm run grunt`

## Inicializando

`$ npm start`

## Rotas e Parametros


## Endpoint

`/signup`

## Metodo HTTP

POST

## Descrição

Cria um novo usuário no banco, utilizando os dados passados nos parametros, retorna o objeto do usuário cadastrado.

## Parametros

| Parameter | Description | Data Type |
|-----------|-----------|-----------|
| nome | Nome do usuário | string |
| email | Email (será utilizado como login do usuário) | string |
| senha | Senha de livre escolha| string |
| telefones | *Opcional*. Inclue uma lista de telefones com DDD.| Array de objetos JSON ex: {"numero": "99999999", "ddd": "99"} |


## Endpoint

`/authenticate`

## Metodo HTTP

POST

## Descrição

Autentica o usuário retornando um token JWT.

## Parametros

| Parameter | Description | Data Type |
|-----------|-----------|-----------|
| email | Email do usuário | string |
| senha | Senha do usuário| string |

## Endpoint

`/search`

## Metodo HTTP

GET

## Descrição

Verifica se o token passado no header é válido e retorna o objeto correspondente ao usuário do token.

