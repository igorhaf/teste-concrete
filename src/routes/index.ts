import { NextFunction, Request, Response, Router } from "express";
import mongoose = require("mongoose");
import { IUser } from "../interfaces/user";
import { IUserModel } from "../models/user";
import { userSchema } from "../schemas/user";
import * as bcrypt from "bcrypt";
import * as jwt from "jsonwebtoken";

/**
 * / route
 *
 * @class User
 */
export class IndexRoute {

  public static create(router: Router) {

    router.post("/signup", (req: Request, res: Response, next: NextFunction) => {
      new IndexRoute().signup(req, res, next);
    });

    router.post("/authenticate", (req: Request, res: Response, next: NextFunction) => {
      new IndexRoute().authenticate(req, res, next);
    });

    router.get("/search", (req: Request, res: Response, next: NextFunction) => {
      new IndexRoute().search(req, res, next);
    });
  }

  public signup(req: Request, res: Response, next: NextFunction) {

    const User = req.app.get("UserModel");
    User.find({email : req.body.email}, function(err, docs) {
      if (docs.length) {
        res.json({ status: 200, message: "Email ja cadastrado" });
      } else {
        User.create(req.body, function(err, doc) {
          if (err) {
            throw err;
          } else {
            const payload = {
              email: doc.email,
              nome: doc.nome,
              _id: doc._id,
            };
            const jwttoken = jwt.sign({ email: doc.email, nome: doc.nome, _id: doc._id}, req.app.get("jwt-secret"), {expiresIn: 1800});
            res.json(
              {
                status: 200,
                message: {
                  id: doc._id,
                  nome: doc.nome,
                  email: doc.email,
                  telefones: doc.telefones,
                  token: jwttoken,
                },
              },
            );
          }
      });
      }
    });
  }

  public search(req: Request, res: Response, next: NextFunction) {
    const authorization: any = req.headers.authorization;
    if (req.headers && authorization && authorization.split(" ")[0] === "Bearer") {
      jwt.verify(authorization.split(" ")[1], req.app.get("jwt-secret"), function(err, decode) {
          if (err) {
            err = {
              name: "TokenExpiredError",
              message: "Sessão inválida",
            };
            res.json({ success: false, message: err });
          }
          if (decode) {
            const User = req.app.get("UserModel");
            User.findOne({
              email: decode.email,
            }, function(err, doc) {
              if (err) {
                throw err;
              } else {
                res.json(
                  {
                    status: 200,
                    message: {
                      id: doc._id,
                      nome: doc.nome,
                      email: doc.email,
                      telefones: doc.telefones,
                    },
                  },
                );
              }
            });
          }
        });
      } else {
        res.json({ status: 401, message: "Não Autorizado" });
      }
  }

  public authenticate(req: Request, res: Response, next: NextFunction) {
    const User = req.app.get("UserModel");
    User.findOne({
      email: req.body.email,
    },           function(err, user) {

    if (err) { throw err; }

    if (!user) {
      res.json({ status: 401, message: "Usuário não encontrado" });
    } else if (user) {
        if (!bcrypt.compareSync(req.body.senha, user.senha)) {
          res.json({ status: 401, message: "Usuário e/ou senha inválidos" });
        } else {

        const payload = {
          email: user.email,
          nome: user.nome,
          _id: user._id,
        };

        const jwttoken = jwt.sign({ email: user.email, nome: user.nome, _id: user._id}, req.app.get("jwt-secret"), {expiresIn: 1800});

        res.json({
          status: 200,
          message: {token: jwttoken},
        });
      }
    }
  });
  }
}
