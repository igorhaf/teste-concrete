import { Schema } from "mongoose";
const bcrypt = require("bcrypt");

const PhoneSchema: Schema = new Schema({
  numero: String,
  ddd: String,
});

export let userSchema: Schema = new Schema({
  nome: String,
  email: String,
  senha: String,
  createdAt: Date,
  telefones: [PhoneSchema],
});

userSchema.pre("save", function(next) {
  if (!this.createdAt) {
    this.createdAt = new Date();
  }
  this.senha = bcrypt.hashSync(this.senha, 10);

  next();
});
